#include "StartMenuController.h"
#include "StartMenuView.h"
#include "LobbyController.h"
USING_NS_CC;
bool StartMenuController::init()
{
    if (!cocos2d::Node::init()) {
        return false;
    }
    view = StartMenuView::create();
    this->addChild(view);
    return true;
}
cocos2d::Scene* StartMenuController::createScene()
{
    auto scene = cocos2d::Scene::create();
    auto node = StartMenuController::create();
    scene->addChild(node);
    return scene;
}
void StartMenuController::handleEvent(int tag)
{
    switch (tag) {
        case 0: //Connect button
            Director::getInstance()->replaceScene(LobbyController::createScene());
            break;
        case 1: //Exit button
            Director::getInstance()->end();
            break;
        default:
            break;
    }
}