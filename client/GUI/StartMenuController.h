#ifndef TECHNOSTARTUP_STARTMENUCONTROLLER_H
#define TECHNOSTARTUP_STARTMENUCONTROLLER_H

#include "StartMenuView.h"
#include "cocos2d.h"

class StartMenuController : public cocos2d::Node {
    StartMenuView *view;
    virtual bool init() override;
//    void update(float delta) override;
public:
    static cocos2d::Scene* createScene();
    void handleEvent(int tag);
    CREATE_FUNC(StartMenuController);
};


#endif //TECHNOSTARTUP_STARTMENUCONTROLLER_H
