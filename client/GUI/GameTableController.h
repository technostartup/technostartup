#ifndef TECHNOSTARTUP_GAMETABLECONTROLLER_H
#define TECHNOSTARTUP_GAMETABLECONTROLLER_H
#include <thread>
#include <queue>
#include <mutex>

#include "cocos2d.h"
#include "GameTableView.h"
#include "ClientNetwork.h"
#include "Event.h"
#include "JoinEvent.h"
#include "ChatEvent.h"

class GameTableController : public cocos2d::Node {
    GameTableView *view;
    virtual bool init() override;
    void update(float delta) override;
    std::queue<Event*> eventQueue; //Объединить с мьютексом и сетью
    std::mutex mtx;
    std::thread networkThread;
    signed char clientNetworkNumber;
    ClientNetwork *clientNetwork;
public:
    static cocos2d::Scene* createScene();
    void sendMessage(signed char uid, const std::string& message);
    CREATE_FUNC(GameTableController);
    ~GameTableController();
};


#endif //TECHNOSTARTUP_GAMETABLECONTROLLER_H
