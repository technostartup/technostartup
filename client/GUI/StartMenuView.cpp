#include <vector>
#include "StartMenuView.h"
#include "StartMenuController.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "Objects/UI.h"
USING_NS_CC;

bool StartMenuView::init() {
    if (!Layer::init()) {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto origin = Director::getInstance()->getVisibleOrigin();
    std::vector<stui::LabeledButton *>buttons;
    buttons.push_back(stui::LabeledButton::create("Resources/Button02.png",
                                                  "Resources/Button03.png",
                                                  "Resources/Button16.png",
                                                  "Подключиться",
                                                  "Resources/fonts/arial.ttf",ui_fontsize));
    buttons.push_back(stui::LabeledButton::create("Resources/Button02.png",
                                                  "Resources/Button03.png",
                                                  "Resources/Button16.png",
                                                  "Выход",
                                                  "Resources/fonts/arial.ttf",ui_fontsize));
    for (int i=0; i<buttons.size(); i++)
    {
        buttons[i]->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - 100*(i+1)));
        buttons[i]->setTag(i);
        buttons[i]->addClickEventListener(CC_CALLBACK_1(StartMenuView::buttonListener,this));
        this->addChild(buttons[i],1);
    }
    return true;
}
void StartMenuView::buttonListener(cocos2d::Ref* pSender)
{
    int tag = static_cast<ui::Button *>(pSender)->getTag();
    auto controller = static_cast<StartMenuController *>(this->getParent());
    controller->handleEvent(tag);
}