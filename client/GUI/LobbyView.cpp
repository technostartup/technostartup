#include <vector>
#include "LobbyView.h"
#include "LobbyController.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "Objects/UI.h"

USING_NS_CC;
using namespace std;
bool LobbyView::init() {
    if (!Layer::init()) {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto sprite = Sprite::create("Resources/Lobby.jpg");
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(sprite, 0);
    Vec2 playerListVertices[] = {Vec2(0,visibleSize.height), Vec2(visibleSize.width/4,visibleSize.height),
                                 Vec2(visibleSize.width/4,0), Vec2(0,0)};
    auto rectWithBorder = DrawNode::create();
    rectWithBorder->setContentSize(cocos2d::Size(visibleSize.width/4,visibleSize.height));
    rectWithBorder->drawPolygon(playerListVertices, 4, Color4F(0,0,0,0.5f), 3, Color4F(0,0,0,0.5f));
    rectWithBorder->setPosition(Vec2(visibleSize.width + origin.x - rectWithBorder->getContentSize().width,
                                     visibleSize.height + origin.y - rectWithBorder->getContentSize().height));
    playerList = ui::ListView::create();
    playerList->setPosition(Vec2(0, 0));
    playerList->setContentSize(rectWithBorder->getContentSize());
    usedPlayerHeight = 0;
    playerList->setInnerContainerSize(cocos2d::Size(rectWithBorder->getContentSize().width,
                                                    rectWithBorder->getContentSize().height));
    rectWithBorder->addChild(playerList,1);
    this->addChild(rectWithBorder,1);
    auto sendBtn = stui::LabeledButton::create("Resources/buttons/SmallBtn.png",
                                               "Resources/buttons/SmallBtn.png",
                                               "Resources/buttons/SmallBtn.png",
                                               "Send",
                                               "Resources/fonts/arial.ttf",
                                               ui_fontsize);
    messageBox = ui::EditBox::create(cocos2d::Size(visibleSize.width*3/4.0f-sendBtn->getContentSize().width-20,
                                                   sendBtn->getContentSize().height), "Resources/messageBox.png");
    messageBox->setFontSize(ui_fontsize);
    messageBox->setPosition(Vec2(origin.x+messageBox->getContentSize().width/2, origin.y+sendBtn->getContentSize().height/2));
    sendBtn->setPosition(Vec2(messageBox->getPosition().x+messageBox->getContentSize().width/2+sendBtn->getContentSize().width/2,
                              messageBox->getPosition().y));
    sendBtn->setTag(0);
    sendBtn->addClickEventListener(CC_CALLBACK_1(LobbyView::buttonListener,this));
    this->addChild(sendBtn,1);
    this->addChild(messageBox,1);
    Vec2 messageListVertices[] = {Vec2(0,visibleSize.height-messageBox->getContentSize().height),
                                  Vec2(messageBox->getContentSize().width+sendBtn->getContentSize().width,visibleSize.height-messageBox->getContentSize().height),
                                  Vec2(messageBox->getContentSize().width+sendBtn->getContentSize().width,0), Vec2(0,0)};
    rectWithBorder = DrawNode::create();
    rectWithBorder->setContentSize(cocos2d::Size(messageBox->getContentSize().width,
                                        visibleSize.height-messageBox->getContentSize().height));
    rectWithBorder->drawPolygon(messageListVertices, 4, Color4F(0,0,0,0.5f), 3, Color4F(0,0,0,0.5f));
    rectWithBorder->setPosition(Vec2(messageBox->getPosition().x-messageBox->getContentSize().width/2,
                                     messageBox->getPosition().y+messageBox->getContentSize().height/2+10));
    messageList = ui::ListView::create();
    messageList->setPosition(Vec2(0, 0));
    messageList->setContentSize(rectWithBorder->getContentSize());
    usedMessageHeight = 0;
    messageList->setInnerContainerSize(cocos2d::Size(rectWithBorder->getContentSize().width,
                                                     rectWithBorder->getContentSize().height*2));
    rectWithBorder->addChild(messageList,1);
    this->addChild(rectWithBorder,1);
    return true;
}
void LobbyView::buttonListener(cocos2d::Ref* pSender)
{
    int tag = static_cast<ui::Button *>(pSender)->getTag();
    auto controller = static_cast<LobbyController *>(this->getParent());
    controller->handleEvent(tag);
}
void LobbyView::addMessage(const string &message)
{
    auto messageLabel = Label::createWithTTF(message, "Resources/fonts/arial.ttf", ui_fontsize);
    float height = messageLabel->getContentSize().height+2;
    if (height+usedMessageHeight>messageList->getInnerContainerSize().height) {
        messageList->setInnerContainerSize(cocos2d::Size(messageList->getInnerContainerSize().width,
                                           messageList->getInnerContainerSize().height*2));
        messageList->setInnerContainerPosition(Vec2(0,0));
    }
    auto children = messageList->getChildren();
    for (auto it = children.begin(); it!=children.end(); ++it)
    {
        (*it)->setPosition((*it)->getPosition().x,(*it)->getPosition().y+height);
    }
    messageLabel->setPosition(messageLabel->getContentSize().width/2,height/2);
    usedMessageHeight+=height;
    messageList->addChild(messageLabel);
}
void LobbyView::addPlayer(const string &player)
{
    auto playerLabel = Label::createWithTTF(player, "Resources/fonts/arial.ttf", ui_fontsize);
    float height = playerLabel->getContentSize().height+2;
    if (height+usedPlayerHeight>playerList->getInnerContainerSize().height) {
        playerList->setInnerContainerSize(cocos2d::Size(playerList->getInnerContainerSize().width,
                                                         height+usedPlayerHeight));
        auto children = playerList->getChildren();
        for (auto it = children.begin(); it!=children.end(); ++it)
        {
            (*it)->setPosition((*it)->getPosition().x,(*it)->getPosition().y+height);
        }
    }
    playerLabel->setPosition(playerLabel->getContentSize().width/2,
                             playerList->getInnerContainerSize().height-(height/2+usedPlayerHeight));
    usedPlayerHeight+=height;
    playerList->addChild(playerLabel);
}