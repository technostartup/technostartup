#ifndef TECHNOSTARTUP_GAMETABLEVIEW_H
#define TECHNOSTARTUP_GAMETABLEVIEW_H


#include "cocos2d.h"
#include "ui/CocosGUI.h"

class GameTableController;
class GameTableView : public cocos2d::Layer {
    cocos2d::ui::EditBox *messageBox;
public:

    virtual bool init() override;
    void menuCloseCallback(cocos2d::Ref* pSender);
    void buttonListener(cocos2d::Ref* pSender);
    CREATE_FUNC(GameTableView);
};

#endif // TECHNOSTARTUP_GAMETABLEVIEW_H
