#ifndef TECHNOSTARTUP_STARTMENUVIEW_H
#define TECHNOSTARTUP_STARTMENUVIEW_H

#include "cocos2d.h"
#include "ui/CocosGUI.h"

static const int ui_fontsize = 18;
class StartMenuView : public cocos2d::Layer {
    void buttonListener(cocos2d::Ref* pSender);
public:
    virtual bool init() override;
    CREATE_FUNC(StartMenuView);
};


#endif //TECHNOSTARTUP_STARTMENUVIEW_H
