#include "GameTableView.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "GameTableController.h"
#include "Objects/UI.h"
USING_NS_CC;

bool GameTableView::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto closeItem = MenuItemImage::create(
                                           "Resources/CloseNormal.png",
                                           "Resources/CloseSelected.png",
                                           CC_CALLBACK_1(GameTableView::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2(origin.x + visibleSize.width/2,
                           origin.y + visibleSize.height - 100));
    this->addChild(menu, 2);
    auto label = Label::createWithTTF("Стартап pre-alpha", "Resources/fonts/arial.ttf", 24);
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
    this->addChild(label, 1);

    auto sprite = Sprite::create("Resources/TableTop.jpg");
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(sprite, 0);
    auto button = stui::LabeledButton::create("Resources/buttons/btn_enabled.png",
                                              "Resources/buttons/btn_pressed.png",
                                              "Resources/buttons/btn_disabled.png",
                                              "Send Message",
                                              "Resources/fonts/arial.ttf",
                                              18);

    button->setPosition(Vec2(origin.x + visibleSize.width/2,
                             origin.y + visibleSize.height - 100));
    button->addClickEventListener(CC_CALLBACK_1(GameTableView::buttonListener,this));
    this->addChild(button,3);
    messageBox = ui::EditBox::create(cocos2d::Size(600, 50),"Resources/messageBox.png");
    messageBox->setPosition(Vec2(origin.x + visibleSize.width/2,
                              origin.y + visibleSize.height - 200));
    this->addChild(messageBox,4);
    return true;
}
void GameTableView::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void GameTableView::buttonListener(cocos2d::Ref* pSender)
{
    auto controller = dynamic_cast<GameTableController *>(this->getParent());
    controller->sendMessage(7,messageBox->getText());
    messageBox->setText("");
}