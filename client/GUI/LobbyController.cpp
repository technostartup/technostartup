#include "LobbyController.h"
#include "StartMenuController.h"
#include "LobbyView.h"
USING_NS_CC;
bool LobbyController::init()
{
    if (!cocos2d::Node::init()) {
        return false;
    }
    view = LobbyView::create();
    this->addChild(view);
    return true;
}
cocos2d::Scene* LobbyController::createScene()
{
    auto scene = cocos2d::Scene::create();
    auto node = LobbyController::create();
    scene->addChild(node);
    return scene;
}
void LobbyController::handleEvent(int tag) {
    switch(tag)
    {
        case 0: //send message
            view->addMessage(view->messageBox->getText());
            view->messageBox->setText("");
            break;
        case 1: //exit lobby
            Director::getInstance()->replaceScene(StartMenuController::createScene());
            break;
        default:
            break;
    }
}