#ifndef TECHNOSTARTUP_LOBBYCONTROLLER_H
#define TECHNOSTARTUP_LOBBYCONTROLLER_H


#include "LobbyView.h"
#include "cocos2d.h"

class LobbyController : public cocos2d::Node {
    LobbyView *view;
    virtual bool init() override;
public:
    static cocos2d::Scene* createScene();
    void handleEvent(int tag);
    CREATE_FUNC(LobbyController);
};


#endif //TECHNOSTARTUP_LOBBYCONTROLLER_H
