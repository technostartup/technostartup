#ifndef TECHNOSTARTUP_LOBBYVIEW_H
#define TECHNOSTARTUP_LOBBYVIEW_H

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class LobbyView : public cocos2d::Layer {
    void buttonListener(cocos2d::Ref* pSender);
    static const int ui_fontsize = 12;
    float usedMessageHeight;
    float usedPlayerHeight;
public:
    cocos2d::ui::ListView *playerList;
    cocos2d::ui::ListView *messageList;
    cocos2d::ui::EditBox *messageBox;
    void addMessage(const std::string &message);
    void addPlayer(const std::string &player);
    virtual bool init() override;
    CREATE_FUNC(LobbyView);
};
#endif //TECHNOSTARTUP_LOBBYVIEW_H
