#include <thread>
#include <queue>
#include <iostream>
#include "cocos2d.h"
#include "GameTableController.h"
#include "ClientNetwork.h"

using namespace std;

bool GameTableController::init()
{
    if (!cocos2d::Node::init()) {
        return false;
    }
    view = GameTableView::create();
    this->addChild(view);
    clientNetworkNumber = -1;
    clientNetwork = ClientNetwork::getInstance();
    this->scheduleUpdate();
    return true;
}
void GameTableController::update(float delta)
{

    Event* event = clientNetwork->getEvent();
    if (event) {
        switch(event->getEventType()) {
            case Event::JOIN_EVENT: {
                JoinEvent *je = dynamic_cast<JoinEvent *>(event); //static_cast
                if (clientNetworkNumber == -1) {
                    clientNetworkNumber = je->getPlayerNumber();
                    cout << "Successfully connected to server. My number is " << static_cast<int>(clientNetworkNumber) << endl;
                } else {
                    cout << "New player " << je->getPlayerName() << " with number " << static_cast<int>(je->getPlayerNumber()) << endl;
                }
                break;
            }
            case Event::CHAT_EVENT: {
                ChatEvent *ce = dynamic_cast<ChatEvent *>(event);
                if (ce->getFrom() == clientNetworkNumber) {
                    cout << "Successfully sent message" << endl;
                } else {
                    cout << (ce->getFrom() != -1 ? "Public" : "Private") <<
                         "message '" << ce->getMessage() << "' from " << static_cast<int>(ce->getFrom()) << endl;
                }
            }
                break;
        }
    }
}
cocos2d::Scene* GameTableController::createScene()
{
    auto scene = cocos2d::Scene::create();
    auto node = GameTableController::create();
    scene->addChild(node);
    return scene;
}
GameTableController::~GameTableController() {}

void GameTableController::sendMessage(signed char uid, const std::string& message)
{
    ChatEvent ce;
    ce.setFrom(clientNetworkNumber);
    ce.setMessage(message);
    ce.setReceiver(uid);
    clientNetwork->send(&ce);
}