#ifndef TECHNOSTARTUP_UI_H
#define TECHNOSTARTUP_UI_H

#include <vector>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

namespace stui
{
    class LabeledButton : public cocos2d::ui::Button
    {
    public:
        cocos2d::Label *label;
        virtual bool init(
                const std::string &,
                const std::string &,
                const std::string &,
                const std::string &,
                const std::string &,
                int);
        static LabeledButton* create(
                const std::string &,
                const std::string &,
                const std::string &,
                const std::string &,
                const std::string &,
                int);
    };
//    class LabelList : public cocos2d::Sprite
//    {
//        std::vector<std::string> text;
//        cocos2d::Label
//    public:
//
//    };
}


#endif //TECHNOSTARTUP_UI_H
