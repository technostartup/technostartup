#include "UI.h"
#include "ui/CocosGUI.h"
USING_NS_CC;
using namespace stui;
using namespace std;
bool LabeledButton::init(
        const string &activebtn,
        const string &pressedbtn,
        const string &disabledbtn,
        const string &text,
        const string &font,
        int fontsize)
{
    if ( !ui::Button::init(activebtn,pressedbtn,disabledbtn) )
    {
        return false;
    }
    this->label = Label::createWithTTF(text,font,fontsize);
    this->label->setPosition(this->getBoundingBox().size.width/2,
                             this->getBoundingBox().size.height/2);
    this->addChild(label,1);
    return true;
}

LabeledButton *LabeledButton::create(
        const string &activebtn,
        const string &pressedbtn,
        const string &disabledbtn,
        const string &text,
        const string &font,
        int fontsize)
{
    LabeledButton *pRet = new(std::nothrow) LabeledButton();
    if (pRet && pRet->init(activebtn,pressedbtn,disabledbtn,text,font,fontsize))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        return nullptr;
    }
}