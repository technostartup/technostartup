//
// Created by Armijo on 16.11.16.
//

#include "ClientNetwork.h"
#include "Event.h"
#include "JoinEvent.h"
#include "NetworkConverter.h"

#include <utility>
#include <iostream>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

static ClientNetwork::instance = NULL;

ClientNetwork* ClientNetwork::getInstance() {
    if (!instance) {
        instance = new ClientNetwork();
    }
    return instance;
}

ClientNetwork::ClientNetwork() :
    socket(io_service)
{
}

void ClientNetwork::init(const string& address, const string& playerName) {
    this->address = address;
    this->playerName = playerName;
    networkThread = thread(&ClientNetwork::listen, this);
}

void ClientNetwork::listen() {
    tcp::resolver resolver(io_service);
    tcp::resolver::iterator iter = resolver.resolve({ address, "9889" });
    connect(iter);
    io_service.run();
}

void ClientNetwork::stopListening() {
    networkThread.detach();
}
void ClientNetwork::connect(tcp::resolver::iterator iter) {
    async_connect(socket, iter, [this](boost::system::error_code ec, tcp::resolver::iterator) {
        if (!ec)
        {
            cout << "Successfull connected to server" << endl;
            Network::init(&socket);
            JoinEvent je;
            je.setFrom(-1);
            je.setPlayerName(playerName);
            Network::send(&je);
            cout << "Sent information about me" << endl;
        };
    });
}

Event* ClientNetwork::getEvent()
{
    Event *event = NULL;
    mtx.lock();
    if (!eventQueue.empty()) {
        event = eventQueue.front(); //TODO: unique_ptr
        eventQueue.pop();
    }
    mtx.unlock();
    return event;
}

void ClientNetwork::callback(Event* event) {
    mtx.lock();
    eventQueue.push(event);
    mtx.unlock();
}

ClientNetwork::~ClientNetwork() {

}