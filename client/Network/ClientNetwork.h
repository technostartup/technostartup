//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_CLIENTNETWORK_H
#define TECHNOSTARTUP_CLIENTNETWORK_H

#include "Network.h"

#include <string>
#include <boost/asio.hpp>
#include <queue>
#include <mutex>
#include <thread>

class Event;

class ClientNetwork : public Network {
public:
    static ClientNetwork* getInstance();
    ClientNetwork();

    void init(const std::string& address,
              const std::string& playerName);
    Event* getEvent();
    ~ClientNetwork() override;

protected:
    void callback(Event* event) override;

private:
    void connect(boost::asio::ip::tcp::resolver::iterator iter);
    void listen();
    void stopListening();
private:
    static ClientNetwork* instance;
    std::thread networkThread;
    std::string address;
    std::string playerName;
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket;

    std::queue<Event*> eventQueue;
    std::mutex mtx;
};


#endif //TECHNOSTARTUP_CLIENTNETWORK_H
