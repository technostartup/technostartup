#ifndef TECHNOSTARTUP_INCIDENTCARD_H
#define TECHNOSTARTUP_INCIDENTCARD_H

#include "Card.h"
#include "Effect.h"

class IncidentCard: public Card {
public:
    IncidentCard();
    IncidentCard(short, bool, Effect);
    short getPayoff() const;
    bool backupSaves() const;
    Effect getEffect() const;

    void serialize(unsigned char* src) const override;
    static Card* deserialize(unsigned char* src, unsigned length);

    unsigned getTotalSize() const override;
    
private:
    short payoff;               //if < 0 then impossibru to pay off
    bool backup;
    Effect effect;
};

#endif
