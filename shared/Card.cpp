#include "Card.h"
#include "ProjectCard.h"
#include "OppCard.h"
#include "IncidentCard.h"
#include "NetworkConverter.h"

Card::Card(CardType pType) : type(pType) {
    
}

Card::CardType Card::getType() const {
    return type;
}


std::string Card::typeToString(const CardType type) const {
    switch(type) {
        case CardType :: PROJECT:
            return "PROJECT";
        case CardType :: OPP:
            return "OPP";
        case CardType :: INCIDENT:
            return "INCIDENT";
        case CardType :: DUMMY:
            return "DUMMY";
    }
}

const int Card::getCardId() const {
    return cardId;
}

bool Card::operator==(const Card& two) {
    return this->cardId == two.cardId;
}

void Card::setCardId(int id) {
    cardId = id;
}

void Card::serialize(unsigned char* data) const {
    data -= 3;
    NetworkConverter::fromInt(data, cardId, 4);
    --data;
    *data = static_cast<unsigned char>(type);
};

Card* Card::deserialize(unsigned char *src, unsigned length) {
    CardType type = static_cast<CardType>(src[0]);
    ++src;
    --length;
    int id = NetworkConverter::toInt<int>(src, 4);
    Card* ret = nullptr;
    switch(type) {
        case PROJECT:
            ret = ProjectCard::deserialize(src, length);
            break;
        case OPP:
            ret = OppCard::deserialize(src, length);
            break;
        case INCIDENT:
            ret = IncidentCard::deserialize(src, length);
            break;
        default:
            ret = new Card(DUMMY);
            break;
    }
    ret->type = type;
    ret->cardId = id;
    return ret;
}

unsigned Card::getTotalSize() const {
    return 5;
}
