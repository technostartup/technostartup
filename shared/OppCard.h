#ifndef TECHNOSTARTUP_OPPCARD_H
#define TECHNOSTARTUP_OPPCARD_H

#include "Card.h"
#include "Effect.h"

class OppCard : public Card {
public:
    enum OppType {
        STAFF,
        EFFECT
    };
    
    enum EffectTarget {
        SELF,
        OPPONENT,
        ALL             //not sure if needed
    };
    
    OppCard();
    OppCard(OppType, OppType, EffectTarget, EffectTarget, Effect, Effect);
    OppCard(OppType, EffectTarget, Effect);

    Effect getActiveEffect() const;
    void setActiveSide(bool);   // true for "up" side, false for "down" side

    void serialize(unsigned char* src) const override;
    static Card* deserialize(unsigned char* src, unsigned length);

    unsigned getTotalSize() const override;

private:
    OppType upSide;
    OppType downSide;
    
    EffectTarget upTarget;
    EffectTarget downTarget;
    
    Effect upEffect;
    Effect downEffect;
    
    bool activeEffectUp;
};

#endif //TECHNOSTARTUP_OPPCARD_H
