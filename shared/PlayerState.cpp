#include "PlayerState.h"

PlayerState::PlayerState() : cardsInHand(0) {
    
}

PlayerState::~PlayerState() {
    handProjects.clear();
    handOpps.clear();
    activeProjects.clear();
    completedProjects.clear();
    staff.clear();
}

void PlayerState::paySalary() {
    //TODO: enhance method
    if (money - staff.size() > 0) {
        money -= staff.size();
    } else {
        money = 0;
    }
}

void PlayerState::getBenefit() {
    for (ProjectCard& project : completedProjects) {
        if (project.wasJustCompleted()) {
            money += project.getOneTimePayment();
            project.setJustCompleted(false);
        } else {
            // money += 0 in most cases
            money += project.getTurnIncome();
        }
    }
}

void PlayerState::addActiveProject( ProjectCard project ) {
    activeProjects.push_back( project );
}

void PlayerState::addOppCardInHand( OppCard opp ) {
    handOpps.push_back( opp );
}

void PlayerState::addProjectCardInHand( ProjectCard project ) {
    handProjects.push_back( project );
}

void PlayerState::removeProjectFromHand( ProjectCard project ) {
    std::vector<ProjectCard>::iterator iter;
    
    for (iter = handProjects.begin(); iter < handProjects.end(); ++iter ){
        if (*iter == project) {
            handProjects.erase( iter );
            break;
        }
    }
}

void PlayerState::removeOppFromHand( OppCard opp) {
    std::vector<OppCard>::iterator iter;
    
    for (iter = handOpps.begin(); iter < handOpps.end(); ++iter ){
        if (*iter == opp) {
            handOpps.erase( iter );
            break;
        }
    }
}

std::vector<OppCard> PlayerState::getHandOpps() {
    return handOpps;
}

std::vector<ProjectCard> PlayerState::getHandProjects() {
    return handProjects;
}

std::vector<ProjectCard> PlayerState::getActiveProjects() {
    return activeProjects;
}

std::vector<OppCard> PlayerState::getStaff() {
    return staff;
}

bool PlayerState::hasBackup() const {
    return backUp.isComplete();
}

void PlayerState::handleEffect( Effect effect ) {
    // TODO: implement this
}

void PlayerState::addWork() {
    // TODO: implemet this
}
