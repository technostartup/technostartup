#ifndef TECHNOSTARTUP_PLAYERSTATE_H
#define TECHNOSTARTUP_PLAYERSTATE_H

#include <vector>

#include "ProjectCard.h"
#include "OppCard.h"
#include "Effect.h"

class PlayerState {
    std::vector<ProjectCard> handProjects;
    std::vector<OppCard> handOpps;
    
    short cardsInHand;
    short money = 10;
    
    std::vector<ProjectCard> activeProjects;
    std::vector<ProjectCard> completedProjects;
    
    ProjectCard backUp;
    
    std::vector<OppCard> staff;
    
    short projectsFailed = 0;
    
    Effect workToAdd;
    
public:
    PlayerState();
    ~PlayerState();
    
    void addProjectCardInHand( ProjectCard );
    void addOppCardInHand( OppCard );
    void removeOppFromHand( OppCard );
    
    void addActiveProject( ProjectCard );
    void removeProjectFromHand( ProjectCard );
    
    void handleEffect( Effect );
    void addWork();
    
    std::vector<ProjectCard> getHandProjects();
    std::vector<ProjectCard> getActiveProjects();
    std::vector<OppCard> getHandOpps();
    std::vector<OppCard> getStaff();
    
    void getBenefit();
    void paySalary();
    
    bool hasBackup() const;
};

#endif //TECHNOSTARTUP_PLAYERSTATE_H
