//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_NETWORK_H
#define TECHNOSTARTUP_NETWORK_H

#include <boost/asio.hpp>

class Event;

class Network {
public:
    Network();

    void init(boost::asio::ip::tcp::socket *socket);
    void send(Event* event);

    virtual ~Network();

protected:
    virtual void callback(Event* event) = 0;

private:
    void readHeader();
    void readBody(unsigned int length);

    boost::asio::ip::tcp::socket *socket;

    static const unsigned headerLen = 2;
    unsigned char headerBuffer[headerLen];
    unsigned char* bodyBuffer;
    unsigned char* sendBuffer;

};


#endif //TECHNOSTARTUP_NETWORK_H
