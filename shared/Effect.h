#ifndef TECHNOSTARTUP_EFFECT_H
#define TECHNOSTARTUP_EFFECT_H

class Effect {
    int money;
public:
    Effect();
    Effect(int);
    const int getMoney() const;

    void serialize(unsigned char* src) const;
    static Effect* deserialize(unsigned char* src, unsigned length);

    unsigned getTotalSize() const;
};

#endif
