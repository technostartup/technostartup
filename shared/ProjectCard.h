#ifndef TECHNOSTARTUP_PROJECTCARD_H
#define TECHNOSTARTUP_PROJECTCARD_H

#include "Card.h"
#include <map>
#include <vector>

class ProjectCard : public Card {
public:
    enum StaffType {
        MANAGER,
        DESIGNER,
        PROGRAMMER,
        CONTENT,
        ANY
    };
    
    ProjectCard();
    ProjectCard(short, short, std::map<StaffType, std::pair<short,short>>);
    ~ProjectCard();
    bool operator==(const ProjectCard &);
    
    short getDeadline() const;
    std::vector<StaffType> getStaffTypesUsed() const;
    void addWorkDoneFor(StaffType);
    void updateDeadline();
    bool isFailed() const;
    bool isComplete() const;
    bool wasJustCompleted() const;
    void setJustCompleted(bool);
    short getOneTimePayment() const;
    short getTurnIncome() const;
    std::map<StaffType, std::pair<short, short>> getWorkToComplete() const;

    void serialize(unsigned char* src) const override;
    static Card* deserialize(unsigned char* src, unsigned length);

    unsigned getTotalSize() const override;

private:
    short deadline;
    std::map<StaffType, std::pair<short, short>> workToComplete;
    bool failed;
    bool justCompleted;
    short oneTimePayment;
    short turnIncome;

};

#endif
