#include "Effect.h"
#include "NetworkConverter.h"

Effect::Effect(): money(0) {
    
}

Effect::Effect(int money): money(money) {
    
}

const int Effect::getMoney() const {
    return money;
}

void Effect::serialize(unsigned char* data) const {
    data -= 3;
    NetworkConverter::fromInt(data, money, 4);
};

Effect* Effect::deserialize(unsigned char *src, unsigned length) {
    Effect* e = new Effect;
    e->money = NetworkConverter::toInt<int>(src, 4);

    return e;
}

unsigned Effect::getTotalSize() const {
    return 4;
}