//
// Created by Armijo on 18.11.16.
//

#ifndef TECHNOSTARTUP_NETWORKCONVERTER_H
#define TECHNOSTARTUP_NETWORKCONVERTER_H


class NetworkConverter {
public:
    template<typename T>
            static T toInt(const unsigned char* src, unsigned size) {
        T ret = 0;
        for(unsigned i = size; i != 0; --i) {
            ret <<= 8;
            ret |= src[i-1];
        }
        return ret;
    }

    template<typename T>
            static void fromInt(unsigned char* src, T value, unsigned size) {
        for(unsigned i = 0; i != size; ++i) {
            src[i] = value & 0xFF;
            value >>= 8;
        }
    }
};


#endif //TECHNOSTARTUP_NETWORKCONVERTER_H
