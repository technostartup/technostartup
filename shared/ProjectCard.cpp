#include "ProjectCard.h"
#include "NetworkConverter.h"

ProjectCard::~ProjectCard() {
    workToComplete.clear();
}

ProjectCard::ProjectCard(): Card(Card::PROJECT), deadline(0), justCompleted(false), failed(false), oneTimePayment(0), turnIncome(0) {
    
}

ProjectCard::ProjectCard(short deadline, short staffTypesUsed, std::map<StaffType, std::pair<short,short>> workAmounts) :
    Card(Card::PROJECT), deadline(deadline), justCompleted(false), failed(false), oneTimePayment(0), turnIncome(0){
    workToComplete = workAmounts;
}

short ProjectCard::getDeadline() const {
    return deadline;
}

std::vector<ProjectCard::StaffType> ProjectCard::getStaffTypesUsed() const {
    std::vector<StaffType> result;
    for(std::map<StaffType, std::pair<short, short>>::const_iterator iter = workToComplete.begin(); iter != workToComplete.end(); ++iter){
        result.push_back(iter -> first);
    }
    return result;
}

void ProjectCard::addWorkDoneFor(ProjectCard::StaffType staff) {
    ++workToComplete[staff].first;
    
    justCompleted = true;
    for(std::map<StaffType, std::pair<short, short>>::iterator iter = workToComplete.begin(); iter != workToComplete.end(); ++iter){
        if (iter -> second.first < iter -> second.second) {
            justCompleted = false;
            break;
        }
    }
}

void ProjectCard::updateDeadline() {
    --deadline;
    if (deadline == 0) {
        for(std::map<StaffType, std::pair<short, short>>::iterator iter = workToComplete.begin(); iter != workToComplete.end(); ++iter){
            if (iter -> second.first < iter -> second.second) {
                //work done is less than work neccessary to complete
                failed = true;
                return;
            }
        }
    }
}

bool ProjectCard::isFailed() const {
    return failed;
}

bool ProjectCard::isComplete() const {
    for(std::map<StaffType, std::pair<short, short>>::const_iterator iter = workToComplete.begin(); iter != workToComplete.end(); ++iter){
        if (iter -> second.first < iter -> second.second) {
            return false;
        }
    }
    return true;
}

bool ProjectCard::wasJustCompleted() const {
    return justCompleted;
}

void ProjectCard::setJustCompleted(bool toSet) {
    justCompleted = toSet;
}

short ProjectCard::getOneTimePayment() const {
    return oneTimePayment;
}

bool ProjectCard::operator==(const ProjectCard & other) {
    return deadline == other.getDeadline() && failed == other.isFailed() && justCompleted == other.wasJustCompleted() &&
        oneTimePayment == other.getOneTimePayment() && turnIncome == other.getTurnIncome() && workToComplete == other.getWorkToComplete();
}

short ProjectCard::getTurnIncome() const {
    return turnIncome;
}

std::map<ProjectCard::StaffType, std::pair<short, short>> ProjectCard::getWorkToComplete() const {
    return workToComplete;
}

void ProjectCard::serialize(unsigned char* data) const  {
    data -= 5 * workToComplete.size() + 8;
    NetworkConverter::fromInt(data, deadline, 2);
    data += 2;
    *data++ = static_cast<unsigned char>(failed);
    *data++ = static_cast<unsigned char>(justCompleted);
    NetworkConverter::fromInt(data, oneTimePayment, 2);
    data += 2;
    NetworkConverter::fromInt(data, turnIncome, 2);
    data += 2;
    *data++ = static_cast<unsigned char>(workToComplete.size());
    for(auto iter = workToComplete.begin(); iter != workToComplete.end(); ++iter) {
        *data++ = iter->first;
        NetworkConverter::fromInt(data, iter->second.first, 2);
        data += 2;
        NetworkConverter::fromInt(data, iter->second.second, 2);
        data += 2;
    }

    data -= 5 * workToComplete.size() + 8 - 1;
    Card::serialize(data);
};

Card* ProjectCard::deserialize(unsigned char *src, unsigned length) {
    ProjectCard* projectCard = new ProjectCard;
    projectCard->deadline = NetworkConverter::toInt<short>(src, 2);
    src += 2;
    length -= 2;
    --length;
    projectCard->failed = *src++;
    --length;
    projectCard->justCompleted = *src++;
    --length;
    projectCard->oneTimePayment = NetworkConverter::toInt<short>(src, 2);
    src += 2;
    length -= 2;
    projectCard->turnIncome  = NetworkConverter::toInt<short>(src, 2);
    src += 2;
    length -= 2;
    unsigned mapLen = *src++;
    for(unsigned i = 0; i != mapLen; ++i) {
        StaffType sType = static_cast<StaffType>(*src++);
        --length;
        std::pair<short, short> value;
        value.first = NetworkConverter::toInt<short>(src, 2);
        src += 2;
        length -= 2;
        value.second = NetworkConverter::toInt<short>(src, 2);
        src += 2;
        length -= 2;
        projectCard->workToComplete[sType] = value;

    }

    return projectCard;
}

unsigned ProjectCard::getTotalSize() const {
    return 8 + 1 + 5 * workToComplete.size() + Card::getTotalSize();
}