#include "IncidentCard.h"
#include "NetworkConverter.h"

IncidentCard::IncidentCard() : Card(Card::INCIDENT), payoff(0) {
    
}

IncidentCard::IncidentCard(short payoff, bool backupSaves, Effect effect): Card(Card::INCIDENT),
    payoff(payoff), backup(backupSaves), effect(effect){
    
}

short IncidentCard::getPayoff() const{
    return payoff;
}

Effect IncidentCard::getEffect() const {
    return effect;
}

bool IncidentCard::backupSaves() const {
    return backup;
}

void IncidentCard::serialize(unsigned char* data) const {
    effect.serialize(data);
    data -= effect.getTotalSize();
    --data;
    NetworkConverter::fromInt(data, payoff, 2);
    --data;
    *data = static_cast<unsigned char>(backup);
    Card::serialize(data - 1);
};

Card* IncidentCard::deserialize(unsigned char *src, unsigned length) {
    IncidentCard* ic = new IncidentCard;
    ic->backup = *src++;
    --length;
    ic->payoff = NetworkConverter::toInt<short>(src, 2);
    src += 2;
    length -= 2;

    Effect* e = Effect::deserialize(src, length);
    ic->effect = *e;
    delete e;

    return ic;
}

unsigned IncidentCard::getTotalSize() const  {
    return 3 + effect.getTotalSize() + Card::getTotalSize();
}
