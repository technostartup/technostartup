#include "OppCard.h"

OppCard::OppCard(): Card(Card::OPP) {
    
}

OppCard::OppCard(OppCard::OppType upType, OppCard::OppType downType, OppCard::EffectTarget upTarget, OppCard::EffectTarget downTarget,
                 Effect upEffect, Effect downEffect):
    Card(Card::OPP), upSide(upType), downSide(downType), upTarget(upTarget), downTarget(downTarget),
    upEffect(upEffect), downEffect(downEffect), activeEffectUp(true){
    
}

OppCard::OppCard(OppCard::OppType upType, OppCard::EffectTarget upTarget, Effect upEffect):
    Card(Card::OPP), upSide(upType), upTarget(upTarget), upEffect(upEffect), activeEffectUp(true) {
    
}

void OppCard::setActiveSide(bool up) {
    activeEffectUp = up;
}

Effect OppCard::getActiveEffect() const {
    return activeEffectUp ? upEffect : downEffect;
}

void OppCard::serialize(unsigned char* data) const {
    upEffect.serialize(data);
    data -= upEffect.getTotalSize();
    downEffect.serialize(data);
    data -= downEffect.getTotalSize();
    data -= 4;
    data[0] = upSide;
    data[1] = downSide;
    data[2] = upTarget;
    data[3] = downTarget;
    data[4] = static_cast<unsigned char>(activeEffectUp);
    Card::serialize(data - 1);
};

Card* OppCard::deserialize(unsigned char *src, unsigned length) {
    OppCard* opp = new OppCard;
    opp->upSide = static_cast<OppType>(*src++);
    opp->downSide = static_cast<OppType>(*src++);
    opp->upTarget = static_cast<EffectTarget>(*src++);
    opp->downTarget = static_cast<EffectTarget>(*src++);
    opp->activeEffectUp = static_cast<bool>(*src++);
    src += 5;
    Effect* e = Effect::deserialize(src, length);
    opp->downEffect = *e;
    src += e->getTotalSize();
    length -= e->getTotalSize();
    delete e;
    e = Effect::deserialize(src, length);
    opp->upEffect = *e;

    return opp;
}

unsigned OppCard::getTotalSize() const  {
    return 5 + upEffect.getTotalSize() + downEffect.getTotalSize() + Card::getTotalSize();
}
