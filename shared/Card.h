#ifndef TECHNOSTARTUP_CARD_H
#define TECHNOSTARTUP_CARD_H

#include <string>

class Card {
    
public:
    
    enum CardType {
        PROJECT,
        OPP,
        INCIDENT,
        DUMMY
    };
    
    Card(CardType);
    CardType getType() const;

    const int getCardId() const;
    std::string typeToString( const CardType ) const;
    bool operator==(const Card&);
    void setCardId(int);

    virtual void serialize(unsigned char* src) const;
    static Card* deserialize(unsigned char* src, unsigned length);

    virtual unsigned getTotalSize() const;
private:
    CardType type;
    int cardId;
};

#endif
