//
// Created by Armijo on 16.11.16.
//

#include <utility>
#include <algorithm>

#include "Network.h"
#include "Event.h"
#include "NetworkConverter.h"

using namespace std;

Network::Network()
{

}

void Network::send(Event* event) {
    unsigned len = event->getTotalSize();
    len += 2;
    sendBuffer = new unsigned char [len];
    event->serialize(sendBuffer + len - 1);
    NetworkConverter::fromInt(sendBuffer, len - 2, 2);

    async_write(*socket, boost::asio::buffer(sendBuffer, len), [this] (boost::system::error_code ec, std::size_t){
        //TODO:
        delete [] sendBuffer;
    });
}

void Network::init(boost::asio::ip::tcp::socket *socket) {
    this->socket = socket;
    readHeader();
}

void Network::readHeader() {
    async_read(*socket, boost::asio::buffer(headerBuffer, headerLen), [this] (boost::system::error_code ec, std::size_t) {
        if (!ec) {
            unsigned length = NetworkConverter::toInt<unsigned>(headerBuffer, headerLen);
            //cout << "Received header with " << length << " length" << endl;
            readBody(length);
        }
    });

}

void Network::readBody(unsigned length) {
    bodyBuffer = new unsigned char[length];
    async_read(*socket, boost::asio::buffer(bodyBuffer, length), [this, length] (boost::system::error_code ec, std::size_t) {
        if (!ec) {
            //cout << "Received body" << endl;
            Event* event = Event::deserialize(bodyBuffer, length);
            delete [] bodyBuffer;
            callback(event);
            delete event;
            readHeader();
        }
    });
}

Network::~Network() {

}