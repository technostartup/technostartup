#include <iostream>

#include "ServerNetwork.h"
#include "ActionHandler.h"

using namespace std;

int main() {
    cout << "Hello, World from server!" << endl;

    ActionHandler actionHandler(4);
    ServerNetwork serverNetwork("0.0.0.0", 4, &actionHandler);
    actionHandler.setServerNetwork(&serverNetwork);

    serverNetwork.init();
    return 0;
}