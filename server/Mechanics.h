//
// Created by Armijo on 16.11.16.
//

// Game rules can be found here: https://s3-eu-west-1.amazonaws.com/mosigra.product.other/510/316/startup.pdf

#ifndef TECHNOSTARTUP_MECHANICS_H
#define TECHNOSTARTUP_MECHANICS_H

#include "EventDifference.h"
#include "PlayerState.h"
#include "IncidentCard.h"
#include "GameEvent.h"
#include "MechanicsTester.h"

#include <vector>
#include <stack>

class Mechanics {
    friend class MechanicsTester;
public:
    Mechanics();
	Mechanics(short);
    ~Mechanics();

    EventDifference changeState(GameEvent* gameEvent);

    const int ON_ALL = -1;
    
private:
    enum States {
        BEGIN_ROUND,
        PROJECT_TAKING,
        OPP_TAKING,
        INCIDENT_TAKING,
        INCIDENT_PAYOFF,
        CHOOSING_PROJECT,
        CHOOSING_OPP,
        SPREADING_WORK
    };
    std::string stateToString( const States ) const;

    States state;

	short numPlayers;
    short iterationsLeft;
    
    short payoff;
    IncidentCard lastIncident;
    
    std::vector<PlayerState> players;
    std::vector<PlayerState>::iterator currentPlayer;
    
    std::stack<ProjectCard> projects;
    std::stack<OppCard> opps;
    std::stack<IncidentCard> incidents;
    
    bool inAcceptableStateFor(const GameEvent* gameEvent) const;
    
    void handleTurnProject(GameEvent *, EventDifference *);
    void handleTurnIncident(GameEvent *, EventDifference *);
    void handleIncidentPayoff(GameEvent *, EventDifference *);
    void handleCardTaking(GameEvent *, EventDifference *);
    void handleProjectPlay(GameEvent *, EventDifference *);
    void handleOppPlay(GameEvent *, EventDifference *);
    void handleSpreadWork(GameEvent *, EventDifference *);
    
    void nextState();
    
    //TODO: need to add a method for creating a vector of all game cards from file
};


#endif //TECHNOSTARTUP_MECHANICS_H
