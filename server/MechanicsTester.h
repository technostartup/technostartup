#ifndef TECHNOSTARTUP_MECHANICSTESTER_H
#define TECHNOSTARTUP_MECHANICSTESTER_H

#include "Mechanics.h"

class MechanicsTester {
    short numPlayers;
    int testTurnProject1();
    int testTurnProject1stTaken();
    int testTurnProjectTaken();
    int testOppTaking();
    int testIncidentTaking();
    int testSuccessfulIncidentPayoff();
    
public:
    MechanicsTester(short);
    
    void runTests();
    short getNumPlayers();
};

#endif
