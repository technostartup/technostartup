//
// Created by Armijo on 16.11.16.
//

#include "Mechanics.h"
#include "Effect.h"

Mechanics::Mechanics() {

}

Mechanics::~Mechanics() {
    players.clear();
    while(!projects.empty()){
        projects.pop();
    }
    while(!opps.empty()){
        opps.pop();
    }
    while(!incidents.empty()){
        incidents.pop();
    }
}

Mechanics::Mechanics(short numPlayers) : numPlayers(numPlayers), iterationsLeft(numPlayers), state(Mechanics::PROJECT_TAKING) {
	for (short i = 0; i < numPlayers; ++i) {
		players.push_back(PlayerState());
    }
    currentPlayer = players.begin();
}

std::string Mechanics::stateToString(const Mechanics::States state) const {
    switch(state) {
        case Mechanics :: BEGIN_ROUND:
            return "BEGIN_ROUND";
        case Mechanics :: PROJECT_TAKING:
            return "PROJECT_TAKING";
        case Mechanics :: OPP_TAKING:
            return "OPP_TAKING";
        case Mechanics :: INCIDENT_TAKING:
            return "INCIDENT_TAKNING";
        case Mechanics :: INCIDENT_PAYOFF:
            return "INCIDENT_PAYOFF";
        case Mechanics :: CHOOSING_PROJECT:
            return "CHOOSING_PROJECT";
        case Mechanics :: CHOOSING_OPP:
            return "CHOOSING_OPP";
        case Mechanics :: SPREADING_WORK:
            return "SPREADING_WORK";
    }
}

bool Mechanics::inAcceptableStateFor(const GameEvent* gameEvent) const{
    GameEvent :: GameEventType type = gameEvent -> getType();
    switch (state){
        case Mechanics :: BEGIN_ROUND:
            // should not happen
            return false;
        case Mechanics :: PROJECT_TAKING:
            if ( type != GameEvent :: GameEventType :: TURN_PROJECT) {
                return false;
            }
            break;
        case Mechanics :: OPP_TAKING:
            if ( type != GameEvent :: GameEventType :: CARD_TAKING) {
                return false;
            }
            break;
        case Mechanics :: INCIDENT_TAKING:
            if ( type != GameEvent :: GameEventType :: TURN_INCIDENT) {
                return false;
            }
            break;
        case Mechanics :: INCIDENT_PAYOFF:
            if ( type != GameEvent :: GameEventType :: INCIDENT_PAYOFF) {
                return false;
            }
            break;
        case Mechanics :: CHOOSING_PROJECT:
            if (type != GameEvent :: GameEventType :: PROJECT_PLAY) {
                return false;
            }
            break;
        case Mechanics :: CHOOSING_OPP:
            if (type != GameEvent :: GameEventType :: OPP_PLAY) {
                return false;
            }
            break;
        case Mechanics :: SPREADING_WORK:
            if (type != GameEvent :: GameEventType :: SPREAD_WORK) {
                return false;
            }
            break;
    }
    return true;
}

EventDifference Mechanics::changeState(GameEvent* gameEvent) {
    //TODO: needs testing
    EventDifference result;
    
    if (inAcceptableStateFor(gameEvent)){
        switch (gameEvent -> getType()){
            case GameEvent :: GameEventType :: TURN_PROJECT:
                handleTurnProject( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: TURN_INCIDENT:
                handleTurnIncident( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: INCIDENT_PAYOFF:
                handleIncidentPayoff( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: CARD_TAKING:
                handleCardTaking( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: OPP_PLAY:
                handleOppPlay( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: PROJECT_PLAY:
                handleProjectPlay( gameEvent, &result );
                break;
            case GameEvent :: GameEventType :: SPREAD_WORK:
                handleSpreadWork( gameEvent, &result );
                break;
        }
        
        if (state == Mechanics :: BEGIN_ROUND) {
            for (PlayerState& player : players) {
                player.getBenefit();
                player.addWork();
                player.paySalary();
            }
            
            ++currentPlayer;
            
            if (currentPlayer == players.end()) {
                currentPlayer = players.begin();
            }
            
            nextState();
        }
    } else {
        std::string message = "Bad input event for current state. Event type: "
                            + gameEvent -> typeToString( gameEvent -> getType() )
                            + ". State: " + stateToString(state) + ".\n";
        result.raiseBadDiff(message);
    }
    
    return result;
}

void Mechanics::handleTurnProject(GameEvent * gameEvent, EventDifference * diff) {
    signed char from = gameEvent -> getFrom();
    bool played = false;
    signed char onWho;
    if (! gameEvent -> isTaken() ) {
        
        onWho = (from + 1) % numPlayers;
        --iterationsLeft;
        
        if (iterationsLeft == 0) {
            nextState();
            iterationsLeft = numPlayers;
        }
        
    } else {
        players[ from ].addActiveProject( * static_cast<const ProjectCard *> ( gameEvent -> getCards()[0]) );
        played = true;
        onWho = from;
        nextState();
        iterationsLeft = numPlayers;
    }
    
    // in other words, if a person took it, then he takes it and passes ("plays") it to (on) himself
    // otherwise, he passes ("plays") it to (on) the next person
    diff -> addDiff( Diff(from, gameEvent -> isTaken(), played, onWho, gameEvent -> getCards()[0]) );
    
}

void Mechanics::handleTurnIncident(GameEvent * gameEvent, EventDifference * diff) {
    lastIncident = * static_cast<const IncidentCard *>( gameEvent -> getCards()[0] );
    payoff = 0;
    diff -> addDiff( Diff(gameEvent -> getFrom(), false, true, ON_ALL, gameEvent -> getCards()[0]));
    nextState();
}

void Mechanics::handleIncidentPayoff(GameEvent * gameEvent, EventDifference * diff) {
    signed char from = gameEvent -> getFrom();
    OppCard card = * static_cast<const OppCard *>( gameEvent -> getCards()[0] );
    Effect payment = card.getActiveEffect();
    players[ from ].handleEffect( payment );
    payoff += -(payment.getMoney());
    diff -> addDiff( Diff(from, false, true, from, &card) );
    --iterationsLeft;
    
    if (iterationsLeft == 0) {
        if (payoff < lastIncident.getPayoff()){
            Effect effect = lastIncident.getEffect();
            for (PlayerState& player : players) {
                if (!(lastIncident.backupSaves() && player.hasBackup())){
                    player.handleEffect( effect );
                }
            }
        }
        nextState();
        iterationsLeft = numPlayers;
    }
}

void Mechanics::handleCardTaking(GameEvent * gameEvent, EventDifference * diff) {
    const std::vector<Card *> cards = gameEvent -> getCards();
    signed char from = gameEvent -> getFrom();
    
    for (Card * card : cards) {
        if (card->getType() == Card::PROJECT){
                players[ from ].addProjectCardInHand(* static_cast<const ProjectCard *> (card));
        } else if (card->getType() == Card::OPP){
            players[ from ].addOppCardInHand(* static_cast<const OppCard *> (card));
        } else {
            std::string message = "Invalid card type for the taking: " + card->typeToString( card->getType() );
            diff -> raiseBadDiff( message );
            return;
        }
        diff -> addDiff( Diff(from, true, false, from, card));
    }
    nextState();
}

void Mechanics::handleOppPlay(GameEvent * gameEvent, EventDifference * diff) {
    signed char from = gameEvent -> getFrom();
    signed char to = gameEvent -> getTo();
    OppCard opp = * static_cast<const OppCard *> (gameEvent -> getCards()[0]);
    
    players [ from ].removeOppFromHand( opp );
    players [ to ].handleEffect( opp.getActiveEffect() );
    
    diff -> addDiff( Diff(from, false, true, to, &opp) );
    
    nextState();
}

void Mechanics::handleSpreadWork(GameEvent * gameEvent, EventDifference * diff) {
    // TODO: implement this
    nextState();
}

void Mechanics::handleProjectPlay(GameEvent * gameEvent, EventDifference * diff) {
    signed char from = gameEvent -> getFrom();
    ProjectCard project = * static_cast<const ProjectCard *> (gameEvent -> getCards()[0]);
    
    players [ from ].addActiveProject( project );
    players [ from ].removeProjectFromHand( project );
    
    diff -> addDiff( Diff(from, false, true, from, gameEvent -> getCards()[0]) );
    nextState();
}

void Mechanics::nextState() {
    switch (state) {
        case Mechanics :: BEGIN_ROUND:
            state = Mechanics :: PROJECT_TAKING;
            break;
        case Mechanics :: PROJECT_TAKING:
            state = Mechanics :: OPP_TAKING;
            break;
        case Mechanics :: OPP_TAKING:
            state = Mechanics :: INCIDENT_TAKING;
            break;
        case Mechanics :: INCIDENT_TAKING:
            state = Mechanics :: INCIDENT_PAYOFF;
            break;
        case Mechanics :: INCIDENT_PAYOFF:
            state = Mechanics :: CHOOSING_PROJECT;
            break;
        case Mechanics :: CHOOSING_PROJECT:
            if (iterationsLeft > 0){
                state = Mechanics :: CHOOSING_OPP;
            } else {
                state = Mechanics :: BEGIN_ROUND;
                iterationsLeft = numPlayers;
            }
            break;
        case Mechanics :: CHOOSING_OPP:
            state = Mechanics :: SPREADING_WORK;
            break;
        case Mechanics :: SPREADING_WORK:
            state = Mechanics :: CHOOSING_PROJECT;
            break;
    }
}
