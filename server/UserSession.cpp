//
// Created by Armijo on 18.11.16.
//

#include "UserSession.h"
#include "NetworkConverter.h"
#include "Event.h"
#include "ActionHandler.h"

#include <iostream>

using namespace std;
using namespace boost::asio::ip;
using namespace boost::asio;


UserSession::UserSession(tcp::socket socket, signed char playerNumber, ActionHandler* actionHandler) :
    socket(move(socket)),
    playerNumber(playerNumber),
    actionHandler(actionHandler)
{

}

void UserSession::start() {
    Network::init(&socket);
}

void UserSession::send(Event* event) {
    Network::send(event);
}

void UserSession::callback(Event* event) {
    event->setFrom(playerNumber);
    actionHandler->processAction(event);
}