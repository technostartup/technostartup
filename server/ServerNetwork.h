//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_CLIENTNETWORK_H
#define TECHNOSTARTUP_CLIENTNETWORK_H

#include "Network.h"

#include <string>
#include <vector>
#include <memory>

#include <boost/asio.hpp>

class ActionHandler;
class UserSession;
class EventDifference;

class ServerNetwork {
public:
    ServerNetwork(const std::string& listeningAddress, unsigned usersCount, ActionHandler* actionHandler);

    ~ServerNetwork();

    void init();

    void sendTo(Event* event, signed char receiver);
    void sendAll(Event* event);

private:
    void acceptConnection(boost::asio::ip::tcp::acceptor& acceptor, boost::asio::ip::tcp::socket& socket);

private:
    ActionHandler* actionHandler;
    const std::string& listeningAddress;
    unsigned usersCount;
    std::vector<std::shared_ptr<UserSession>> sessions;
};


#endif //TECHNOSTARTUP_CLIENTNETWORK_H
