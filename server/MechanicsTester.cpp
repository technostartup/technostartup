#include "MechanicsTester.h"
#include "ProjectCard.h"
#include "OppCard.h"
#include "GameEvent.h"
#include "EventDifference.h"

#include <vector>
#include <exception>
#include <iostream>

MechanicsTester::MechanicsTester(short num): numPlayers(num) {
}

short MechanicsTester::getNumPlayers() {
    return numPlayers;
}

void MechanicsTester::runTests() {
    std::cout << "TurnProject with one iter return code: " << testTurnProject1() << std::endl;
    std::cout << "TurnProject with 1st player taking return code: " << testTurnProject1stTaken() << std::endl;
    std::cout << "TurnProject with last player taking return code: " << testTurnProjectTaken() << std::endl;
    std::cout << "OppTaking for active players return code: " << testOppTaking() << std::endl;
    std::cout << "IncidentTaking return code: " << testIncidentTaking() << std::endl;
    std::cout << "SuccessfulIncidentPayoff return code: " << testSuccessfulIncidentPayoff() << std::endl;
}

int MechanicsTester::testTurnProject1() {
    Mechanics ut(numPlayers);
    ProjectCard card;
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    GameEvent event(0, 0, false, cards, GameEvent :: GameEventType :: TURN_PROJECT);
    EventDifference evDiff = ut.changeState(&event);
    if (evDiff.isBadDiff() ||
        ut.state != Mechanics :: States :: PROJECT_TAKING ||
        ut.iterationsLeft != numPlayers - 1) {
        return -1;
    }
    return 0;
}

int MechanicsTester::testTurnProject1stTaken() {
    Mechanics ut(numPlayers);
    ProjectCard card;
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    GameEvent event(0, 0, true, cards, GameEvent :: GameEventType :: TURN_PROJECT);
    EventDifference evDiff = ut.changeState(&event);
    if (evDiff.isBadDiff() ||
        ut.state != Mechanics :: States :: OPP_TAKING ||
        ut.iterationsLeft != numPlayers) {
        return -1;
    }
    return 0;
}

int MechanicsTester::testTurnProjectTaken() {
    Mechanics ut(numPlayers);
    ProjectCard card;
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    for (int i = 0; i < numPlayers; ++i){
        GameEvent event(i, i, i == numPlayers - 1, cards, GameEvent :: GameEventType :: TURN_PROJECT);
        EventDifference evDiff = ut.changeState(&event);
        if (evDiff.isBadDiff() ||
            (ut.state != Mechanics :: States :: PROJECT_TAKING && i < numPlayers - 1) ||
            (ut.state != Mechanics :: States :: OPP_TAKING && i == numPlayers - 1) ||
            (i == numPlayers - 1 && ut.players[i].getActiveProjects().size() != 1)) {
            return -1;
        }
    }
    return 0;
}

int MechanicsTester::testOppTaking() {
    Mechanics ut(numPlayers);
    OppCard card;
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    cards.push_back(&card);
    ut.state = Mechanics :: States :: OPP_TAKING;
    
    GameEvent event(0, 0, true, cards, GameEvent :: GameEventType :: CARD_TAKING);
    EventDifference evDiff = ut.changeState(&event);
    if (evDiff.isBadDiff() ||
        ut.state != Mechanics :: States :: INCIDENT_TAKING ||
        ut.players[0].getHandOpps().size() != 2) {
        return -1;
    }
    return 0;
}

int MechanicsTester::testIncidentTaking() {
    Mechanics ut(numPlayers);
    IncidentCard card;
    card.setCardId(0);
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    GameEvent event(0, 0, true, cards, GameEvent :: GameEventType :: TURN_INCIDENT);
    ut.state = Mechanics :: States :: INCIDENT_TAKING;
    EventDifference evDiff = ut.changeState(&event);
    if (evDiff.isBadDiff() ||
        ut.state != Mechanics :: States :: INCIDENT_PAYOFF ||
        !(card == ut.lastIncident)) {
        return -1;
    }
    return 0;
}

int MechanicsTester::testSuccessfulIncidentPayoff() {
    Mechanics ut(numPlayers);
    IncidentCard card(2, false, Effect());
    card.setCardId(0);
    std::vector<Card*> cards;
    
    cards.push_back(&card);
    GameEvent event(0, 0, true, cards, GameEvent :: GameEventType :: TURN_INCIDENT);
    ut.state = Mechanics :: States :: INCIDENT_TAKING;
    EventDifference evDiff = ut.changeState(&event);
    
    if (evDiff.isBadDiff() ||
        ut.state != Mechanics :: States :: INCIDENT_PAYOFF ||
        !(card == ut.lastIncident)) {
        return -2;
    }
    
    for (int i = 0; i < numPlayers; ++i) {
        OppCard payment(OppCard::OppType::EFFECT, OppCard::EffectTarget::ALL, Effect(2));
        GameEvent payoff(i, i, false, cards, GameEvent :: GameEventType :: INCIDENT_PAYOFF);
        evDiff = ut.changeState(&payoff);
    }
    
    if (ut.state != Mechanics :: States :: CHOOSING_PROJECT) {
        return -1;
    }
    
    return 0;
}
