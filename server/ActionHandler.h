//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_ACTIONHANDLER_H
#define TECHNOSTARTUP_ACTIONHANDLER_H

#include <map>
#include <string>
#include <list>
#include "EventDifference.h"

class Event;
class ServerNetwork;
class Mechanics;

class ActionHandler {
public:
    ActionHandler(unsigned playerCount);
    void processAction(Event* action);
    void setServerNetwork(ServerNetwork* serverNetwork);

private:
    void addToAll(std::vector<EventDifference> &to, Diff ed) const;
    void addToExcl(std::vector<EventDifference> &to, Diff ed, const std::list<signed char>& excluded) const;

private:
    ServerNetwork* serverNetwork;
    Mechanics* mechanics;

    std::map<int, std::string> playerNameNumber;
    unsigned playerCount;
};


#endif //TECHNOSTARTUP_ACTIONHANDLER_H
