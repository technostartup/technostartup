//
// Created by Armijo on 16.11.16.
//

#include "ActionHandler.h"

#include "Event.h"
#include "ChatEvent.h"
#include "JoinEvent.h"
#include "ServerNetwork.h"
#include "Card.h"

#include "Mechanics.h"

#include <iostream>

using namespace std;

ActionHandler::ActionHandler(unsigned playerCount) :
        playerCount(playerCount)
{

}

void ActionHandler::processAction(Event* action) {
    switch(action->getEventType()) {
        case Event::JOIN_EVENT: {
            JoinEvent *joinEvent = dynamic_cast<JoinEvent*>(action);
            unsigned long count = playerNameNumber.size();
            cout << "There is new player '" << joinEvent->getPlayerName() << "' with number " << count << endl;
            playerNameNumber[count] = joinEvent->getPlayerName();
            joinEvent->setPlayerNumber(static_cast<signed char>(count));

            JoinEvent prevJoin;
            prevJoin.setEventType(Event::JOIN_EVENT);
            for(int i = 0; i != count - 1; ++i) {
                prevJoin.setPlayerNumber(i);
                prevJoin.setPlayerName(playerNameNumber[i]);
                serverNetwork->sendTo(&prevJoin, count);
            }
            serverNetwork->sendAll(joinEvent);
            break;
        }
        case Event::GAME_EVENT: {
            GameEvent *gameEvent = dynamic_cast<GameEvent *>(action);

            EventDifference diff = mechanics->changeState(gameEvent);

            vector<EventDifference> toPlayer(playerCount);
            for(auto &difference : diff.getDiffs()) {
                switch (difference.getCard()->getType()) {
                    case Card::INCIDENT:
                        //
                        // Кто-то взял карту инцидента. Все об этом узнают
                        //
                        addToAll(toPlayer, difference);
                        break;
                    case Card::OPP:
                        if (difference.getPlayed()) {
                            //
                            // Кто-то сыграл карту возможностей. Все об этом узнают
                            //
                            addToAll(toPlayer, difference);
                        } else {
                            //
                            // Кто-то взял карту возможностей. Он узнает какую карту
                            //
                            toPlayer[difference.getOnWho()].addDiff(difference);

                            //
                            // Остальные лишь узнают событие
                            //
                            Diff d = difference;
                            d.setCard(nullptr);
                            addToExcl(toPlayer, d, {difference.getOnWho()});
                        }
                        break;
                    case Card::PROJECT:
                        if (difference.getPlayed()) {
                            //
                            // Кто-то сыграл карту проекта. Все об этом узнают
                            //
                            addToAll(toPlayer, difference);
                        } else {
                            if (difference.getOnWho() == difference.getWho()) {
                                //
                                // Кто-то взял карту возможностей. Он узнает какую карту
                                //
                                toPlayer[difference.getOnWho()].addDiff(difference);

                                //
                                // Остальные лишь узнают событие
                                //
                                Diff d = difference;
                                d.setCard(nullptr);
                                addToExcl(toPlayer, d, {difference.getOnWho()});
                            } else {
                                //
                                // Кто-то не взял карту возможностей (передал дальше). Об этом узнает он и кому передали
                                //
                                toPlayer[difference.getOnWho()].addDiff(difference);
                                toPlayer[difference.getWho()].addDiff(difference);

                                //
                                // Остальные лишь узнают событие
                                //
                                Diff d = difference;
                                d.setCard(nullptr);
                                addToExcl(toPlayer, d, {difference.getOnWho(), difference.getWho()});
                            }
                        }
                        break;
                    default:
                            break;
                }
            }
            for(int i = 0; i != toPlayer.size(); ++i) {
                serverNetwork->sendTo(&toPlayer[i], i);
            }
        }
            break;
        case Event::CHAT_EVENT: {
            ChatEvent *chatEvent = dynamic_cast<ChatEvent*>(action);
            if(chatEvent->getReceiver() != -1) {
                serverNetwork->sendTo(chatEvent, chatEvent->getReceiver());
                serverNetwork->sendTo(chatEvent, chatEvent->getFrom());
            } else {
                    serverNetwork->sendAll(chatEvent);
            }
        }
            break;
        case Event::TRADE_EVENT:
            break;
        default:
            //Shouldn't be there;
            break;
    }
}

void ActionHandler::setServerNetwork(ServerNetwork *serverNetwork) {
    this->serverNetwork = serverNetwork;
}

void ActionHandler::addToAll(std::vector<EventDifference> &to, Diff diff) const {
    for(auto& ed : to) {
        ed.addDiff(diff);
    }
}

void ActionHandler::addToExcl(std::vector<EventDifference> &to, Diff ed, const std::list<signed char>& excluded) const {
    for(int i = 0; i != to.size(); ++i) {
        bool isExcluded = false;
        for(auto ex : excluded) {
            if(ex == i) {
                isExcluded = true;
                break;
            }
        }
        if(!isExcluded) {
            to[i].addDiff(ed);
        }
    }

}