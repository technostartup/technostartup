//
// Created by Armijo on 16.11.16.
//

#include "ServerNetwork.h"
#include "ActionHandler.h"
#include "UserSession.h"

#include <iostream>

#include <memory>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

ServerNetwork::ServerNetwork(const string& listeningAddress, unsigned usersCount, ActionHandler* actionHandler) :
        actionHandler(actionHandler),
        listeningAddress(listeningAddress),
        usersCount(usersCount)
{

}

ServerNetwork::~ServerNetwork() {

}

void ServerNetwork::init() {
    io_service service;
    tcp::endpoint endpoint(tcp::v4(), stoi("9889"));
    tcp::acceptor acceptor(service, endpoint);
    tcp::socket socket(service);
    acceptConnection(acceptor, socket);
    service.run();
}

void ServerNetwork::sendTo(Event* event, signed char receiver) {
    if (receiver < 0 || receiver > sessions.size() -1) {
        cerr << "Unexpected user with " << static_cast<int>(receiver) << " id" << endl;
        return;
    }
    sessions[receiver]->send(event);
}

void ServerNetwork::sendAll(Event* event) {
    for(auto session : sessions) {
        session->send(event);
    }
}

void ServerNetwork::acceptConnection(tcp::acceptor& acceptor, tcp::socket& socket) {
    acceptor.async_accept(socket, [this, &socket, &acceptor] (boost::system::error_code ec) {
        if (!ec) {
            cout << "New connection from " << socket.remote_endpoint().address().to_string() << " IP" << endl;
            if (usersCount > 0) {
                sessions.push_back(make_shared<UserSession>(std::move(socket), sessions.size(), actionHandler));
                sessions.back()->start();
            }
        }

        acceptConnection(acceptor, socket);
    });
}