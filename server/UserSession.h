//
// Created by Armijo on 18.11.16.
//

#ifndef TECHNOSTARTUP_USERSESSION_H
#define TECHNOSTARTUP_USERSESSION_H

#include <boost/asio.hpp>
#include <memory>

#include "Network.h"

class ActionHandler;
class Event;

class UserSession : Network {
public:
    UserSession(boost::asio::ip::tcp::socket socket, signed char playerNumber, ActionHandler* actionHandler);
    void start();
    void send(Event* event);

protected:
    void callback(Event* event) override;

private:
    boost::asio::ip::tcp::socket socket;
    signed char playerNumber;

    ActionHandler* actionHandler;
};


#endif //TECHNOSTARTUP_USERSESSION_H
