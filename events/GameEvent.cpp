//
// Created by Armijo on 16.11.16.
//
#include "GameEvent.h"

GameEvent::GameEvent() : Event(GAME_EVENT) {
    
}

GameEvent::GameEvent(signed char pFrom, signed char pTo, bool taken, const std::vector<Card *> & cards, GameEventType pType) :
    Event(GAME_EVENT), to(pTo), type(pType), cards(cards), taken(taken) {
        setFrom(pFrom);
}

GameEvent::GameEventType GameEvent::getType() const {
    return type;
}

const std::vector<Card *> & GameEvent::getCards() const {
    return cards;
}

signed char GameEvent::getTo() const {
    return to;
}

void GameEvent::setType(GameEvent::GameEventType type){
    this->type = type;
}

bool GameEvent::isTaken() const {
    return taken;
}

std::string GameEvent::typeToString(const GameEventType type) const {
    switch (type) {
        case GameEventType :: CARD_TAKING:
            return "CARD_TAKING";
        case GameEventType :: OPP_PLAY:
            return "OPP_PLAY";
        case GameEventType :: PROJECT_PLAY:
            return "PROJECT_PLAY";
        case GameEventType :: TURN_PROJECT:
            return "TURN_PROJECT";
        case GameEventType :: TURN_INCIDENT:
            return "TURN_INCIDENT";
        case GameEventType :: INCIDENT_PAYOFF:
            return "INCIDENT_PAYOFF";
        case GameEventType :: SPREAD_WORK:
            return "SPREAD_WORK";

    }
}


void GameEvent::serialize(unsigned char* data) {
    for(auto* card : cards) {
        unsigned len = card->getTotalSize();
        card->serialize(data);
        data -= len;
        *data-- = static_cast<unsigned char>(len);
    }

    *data-- = static_cast<unsigned char>(cards.size());
    *data-- = static_cast<unsigned char>(to);
    *data-- = type;
    *data-- = static_cast<unsigned char>(taken);
    Event::serialize(data);
};

Event* GameEvent::deserialize(unsigned char *src, unsigned length) {
    GameEvent* gameEvent = new GameEvent;
    gameEvent->taken = src[0];
    ++src;
    --length;
    gameEvent->type = static_cast<GameEventType>(src[0]);
    ++src;
    --length;
    gameEvent->to = src[0];
    ++src;
    --length;
    unsigned char count = *src++;
    for(unsigned char i = 0; i != count; ++i) {
        unsigned char len = *src++;
        --length;
        Card* c = Card::deserialize(src, len);
        src += len;
        length -= len;
        gameEvent->cards.push_back(c);

    }

    return gameEvent;
}

unsigned GameEvent::getTotalSize() {
    unsigned len = 4;
    for(auto card : cards) {
        len += card->getTotalSize() + 1;
    }
    return len;
}
