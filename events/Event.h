//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_EVENT_H
#define TECHNOSTARTUP_EVENT_H

#include <utility>

class Event {
public:
    enum EventType {
        JOIN_EVENT,
        GAME_EVENT,
        CHAT_EVENT,
        TRADE_EVENT
    };

    Event();
    Event(EventType et);
    virtual ~Event() = 0;

    EventType getEventType();
    void setEventType(EventType eventType);
    signed char getFrom();
    void setFrom(signed char from);

    virtual void serialize(unsigned char* data) = 0;

    static Event* deserialize(unsigned char* src, unsigned size);

    virtual unsigned getTotalSize() = 0;

private:
    EventType eventType;
    signed char from;
};


#endif //TECHNOSTARTUP_EVENT_H
