//
// Created by Armijo on 16.11.16.
//
#include <algorithm>
#include "Event.h"
#include "ChatEvent.h"
#include "JoinEvent.h"

using namespace std;

Event::Event() {

}

Event::Event(EventType et) :
    eventType(et)
{

}

Event::~Event() {

}

Event::EventType Event::getEventType() {
    return eventType;
}

void Event::setEventType(Event::EventType eventType) {
    this->eventType = eventType;
}

signed char Event::getFrom() {
    return from;
}

void Event::setFrom(signed char from) {
    this->from = from;
}

void Event::serialize(unsigned char* data) {
    data -= 1;
    data[0] = static_cast<unsigned char>(eventType);
    data[1] = static_cast<unsigned char>(from);
}

Event* Event::deserialize(unsigned char *src, unsigned size) {
    EventType et = static_cast<EventType>(src[0]);
    ++src;
    --size;
    signed char from = static_cast<signed char>(src[0]);
    ++src;
    --size;
    Event* ret = nullptr;
    switch(et) {
        case JOIN_EVENT:
            ret = JoinEvent::deserialize(src, size);
            break;
        case GAME_EVENT:
            break;
        case CHAT_EVENT:
            ret = ChatEvent::deserialize(src, size);
            break;
        case TRADE_EVENT:
            break;
        default:
            // Should not be there
            break;
    }
    ret->setEventType(et);
    ret->setFrom(from);
    return ret;
}

unsigned Event::getTotalSize() {
    return 2;
}