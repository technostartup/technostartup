//
// Created by Armijo on 18.11.16.
//
#include <algorithm>
#include "ChatEvent.h"

using namespace std;

ChatEvent::ChatEvent()
{
    setEventType(CHAT_EVENT);
}

signed char ChatEvent::getReceiver() {
    return receiver;
}

void ChatEvent::setReceiver(signed char receiver) {
    this->receiver = receiver;
}

const string& ChatEvent::getMessage() const {
    return message;
};

void ChatEvent::setMessage(const string& message) {
    this->message = message;
}

void ChatEvent::serialize(unsigned char* data) {
    data -= message.length() + 1;
    data[0] = static_cast<unsigned char>(receiver);
    copy_n(message.c_str(), message.length() + 1, data + 1);
    Event::serialize(data - 1);
};

Event* ChatEvent::deserialize(unsigned char *src, unsigned length) {
    ChatEvent* chatEvent = new ChatEvent;
    chatEvent->receiver = static_cast<signed char>(src[0]);
    ++src;
    --length;
    chatEvent->message = reinterpret_cast<char*>(src);

    return chatEvent;
}

unsigned ChatEvent::getTotalSize() {
    return message.size() + 2 + Event::getTotalSize();
}