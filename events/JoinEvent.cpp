//
// Created by Armijo on 18.11.16.
//
#include <algorithm>
#include "JoinEvent.h"

using namespace std;

JoinEvent::JoinEvent() {
    setEventType(JOIN_EVENT);
}

const string& JoinEvent::getPlayerName() const {
    return playerName;
}

void JoinEvent::setPlayerName(const std::string& playerName) {
    this->playerName = playerName;
}

signed char JoinEvent::getPlayerNumber() const {
    return playerNumber;
}

void JoinEvent::setPlayerNumber(signed char playerNumber) {
    this->playerNumber = playerNumber;
}

void JoinEvent::serialize(unsigned char* data) {
    data -= playerName.length() + 1;
    data[0] = static_cast<unsigned char>(playerNumber);
    copy_n(playerName.c_str(), playerName.length() + 1, data + 1);
    Event::serialize(data - 1);
};

Event* JoinEvent::deserialize(unsigned char *src, unsigned len) {
    JoinEvent* joinEvent = new JoinEvent;
    joinEvent->playerNumber = static_cast<signed char>(src[0]);
    ++src;
    --len;
    joinEvent->playerName = string(reinterpret_cast<char*>(src), len - 1);
    return joinEvent;
}

unsigned JoinEvent::getTotalSize() {
    return playerName.length() + 2 + Event::getTotalSize();
}