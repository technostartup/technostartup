#ifndef TECHNOSTARTUP_DIFF_H
#define TECHNOSTARTUP_DIFF_H

#include "Card.h"

class Diff {
public:
    Diff(signed char, bool, bool, signed char, const Card * const);

    signed char getWho() const;
    bool getTook() const;
    bool getPlayed() const;
    bool getOnWho() const;
    const Card* getCard() const;

    void setCard(const Card* card);


    virtual void serialize(unsigned char* src);
    static Diff* deserialize(unsigned char* src, unsigned length);

    virtual unsigned getTotalSize();

private:
    signed char who;
    bool took;
    bool played;
    signed char onWho;
    const Card* card;
};

#endif
