//
// Created by Armijo on 16.11.16.
//

#include "EventDifference.h"

EventDifference::EventDifference() : badDiff(false) {
    
}

EventDifference::~EventDifference() {
    diffs.clear();
}

void EventDifference::raiseBadDiff( std::string error) {
    badDiff = true;
    message = error;
}

bool EventDifference::isBadDiff() {
    return badDiff;
}

void EventDifference::addDiff( Diff diff ) {
    diffs.push_back( diff );
}

std::string EventDifference::getMessage() {
    return message;
}

const std::vector<Diff>& EventDifference::getDiffs() const {
    return diffs;
}

void EventDifference::serialize(unsigned char* data) {
    data -= getTotalSize() - 1;
    data[0] = static_cast<unsigned char>(badDiff);
    data[1] = static_cast<unsigned char>(diffs.size());
    data += 2;
    for(auto& diff : diffs) {
        unsigned char len = diff.getTotalSize();
        *data++ = len;
        diff.serialize(data);
        data += len;
    }

};

EventDifference* EventDifference::deserialize(unsigned char *src, unsigned length) {
    EventDifference* ed = new EventDifference;
    ed->badDiff = *src++;
    --length;
    unsigned char count = *src++;
    --length;
    for(unsigned char i = 0; i != count; ++i) {
        unsigned char len = *src++;
        --length;
        Diff *d = Diff::deserialize(src, len);
        ed->diffs.push_back(*d);
        delete d;
        src += len;
        length -= len;
    }

    return ed;
}

unsigned EventDifference::getTotalSize() {
    unsigned len = 2;
    for(auto &diff : diffs) {
        len += diff.getTotalSize();
    }
    return len;
}