#include "Diff.h"

Diff::Diff(signed char who, bool took, bool played, signed char onWho, const Card * const card):
who(who), took(took), played(played), onWho(onWho) {
    if (card != nullptr) {
        this->card = card;
    } else {
        this->card = new Card(Card::DUMMY);
    }
}

signed char Diff::getWho() const {
    return who;
}

bool Diff::getTook() const {
    return took;
}
bool Diff::getPlayed() const {
    return played;
}
bool Diff::getOnWho() const {
    return onWho;
}

const Card* Diff::getCard() const {
    return card;
}

void Diff::setCard(const Card* card) {
    this->card = card;
}

void Diff::serialize(unsigned char* data) {
    if (card != nullptr) {
        card->serialize(data);
    }

    data -= getTotalSize() - 1;

    data[0] = static_cast<unsigned char>(who);
    data[1] = static_cast<unsigned char>(took);
    data[2] = static_cast<unsigned char>(played);
    data[3] = static_cast<unsigned char>(onWho);
    data[4] = static_cast<unsigned char>(card ? 1 : 0);
};

Diff* Diff::deserialize(unsigned char *src, unsigned length) {
    Card* c = nullptr;
    if (src[4]) {
        c = Card::deserialize(src + 4, length - 4);
    }
    Diff* ret = new Diff(src[0], src[1], src[2], src[3], c);
    delete c;
    return ret;
}

unsigned Diff::getTotalSize() {
    return 5 + (card ? card->getTotalSize() : 0);
}