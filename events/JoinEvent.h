//
// Created by Armijo on 18.11.16.
//

#ifndef TECHNOSTARTUP_JOINEVENT_H
#define TECHNOSTARTUP_JOINEVENT_H

#include "Event.h"

#include <string>

class JoinEvent : public Event {
public:
    JoinEvent();

    const std::string& getPlayerName() const;
    void setPlayerName(const std::string& playerName);

    signed char getPlayerNumber() const;
    void setPlayerNumber(signed char playerNumber);

    void serialize(unsigned char* data ) override;
    static Event* deserialize(unsigned char* src, unsigned len);

    unsigned getTotalSize() override;

private:
    std::string playerName;
    signed char playerNumber;
};


#endif //TECHNOSTARTUP_JOINEVENT_H
