//
// Created by Armijo on 18.11.16.
//

#ifndef TECHNOSTARTUP_CHATEVENT_H
#define TECHNOSTARTUP_CHATEVENT_H

#include "Event.h"

#include <string>

class ChatEvent : public Event{
public:
    ChatEvent();
    signed char getReceiver();
    void setReceiver(signed char receiver);
    const std::string& getMessage() const;
    void setMessage(const std::string& message);

    void serialize(unsigned char* src) override;
    static Event* deserialize(unsigned char* src, unsigned length);

    unsigned getTotalSize() override;
private:

    signed char receiver;
    std::string message;
};


#endif //TECHNOSTARTUP_CHATEVENT_H
