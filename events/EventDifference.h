//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_EVENTDIFFERENCE_H
#define TECHNOSTARTUP_EVENTDIFFERENCE_H

#include <vector>
#include <string>

#include "Diff.h"
#include "Event.h"

class EventDifference : public Event {
public:
    EventDifference();
    ~EventDifference();
    
    void raiseBadDiff( std::string );
    void addDiff( Diff );
    bool isBadDiff();
    std::string getMessage();
    const std::vector<Diff>& getDiffs() const;

    virtual void serialize(unsigned char* src);
    static EventDifference* deserialize(unsigned char* src, unsigned length);

    virtual unsigned getTotalSize();
    
private:
    bool badDiff;
    std::string message;
    std::vector<Diff> diffs;
};


#endif //TECHNOSTARTUP_EVENTDIFFERENCE_H
