//
// Created by Armijo on 16.11.16.
//

#ifndef TECHNOSTARTUP_GAMEEVENT_H
#define TECHNOSTARTUP_GAMEEVENT_H

#include <string>
#include <vector>

#include "Event.h"
#include "Card.h"

class GameEvent : public Event {

public:
	enum GameEventType {
		CARD_TAKING,
		OPP_PLAY,
		PROJECT_PLAY,
		TURN_PROJECT,
		TURN_INCIDENT,
        INCIDENT_PAYOFF,
        SPREAD_WORK
    };

	GameEvent();
    GameEvent(signed char, signed char, bool, const std::vector<Card *> &, GameEventType);

	GameEventType getType() const;
	void setType(GameEventType type);
    std::string typeToString(const GameEventType) const;
    
    signed char getTo() const;
    const std::vector<Card *> & getCards() const;
    bool isTaken() const;

	void serialize(unsigned char* src) override;
	static Event* deserialize(unsigned char* src, unsigned length);

	unsigned getTotalSize() override;

private:
    signed char	to;
    std::vector<Card *> cards;
    GameEventType type;
    bool taken;
};


#endif //TECHNOSTARTUP_GAMEEVENT_H
